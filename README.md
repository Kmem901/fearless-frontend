# Conference Go
A conference and event scheduling application.

## Back-End Design

* Conference Go Microservice:
    * Conference-management project within the monolith directory. Contains apps accounts, events, and presentations.

    ### Accounts
    * Allows for user creation, login, and logout functionalities. Publishes/sends account data to the subscribed services, in this case the attendee microservice. 

    ### Events
    * Conference model, location model, and state model. Allows for CRUD funcitonality. 

    ### Presentations
    * Presentation model and status model. Utilizes RabbitMQ to use queueing to distribute information regarding presentation approval and rejection

* Attendees Microservice:
    * Attendees-management project within the attendees_mircoservice directory. 
    * Utilizes polling to obtain information regarding Conference instances.
    * Utilizes pub/sub to consume published account information from the Conference Go Microservice.

* Presentation Workflow Microservice:
    * Consumer to process presentation approval and rejection output from presentations in the monolith directory. 

## Front-End Design

### Forms
Standard React class components that permit the creation of instances per the title of the component.

### Lists
Standard React class components that permit the listing of instances per the title of the component.

### Detail
Standard React class components that permit the detailing of instances per the title of the component.


## Project Initialization
1. Clone repository to your local machine
2. CD into new project directory
3. Create .env and include the following:
    ```
    OPEN_WEATHER_API_KEY="XXXXXXXXXX"
    PEXELS_API_KEY="YYYYYYYYYY"
    DJWTO_SIGNING_KEY=abcdefg
    ```
    * Replace XXXXXXXXXXXX with your [OPEN WEATHER API](https://openweathermap.org/api) key <br />
    * Replace YYYYYYYYYYYY with your [PEXELS API](https://www.pexels.com/api/) key
4. Start docker
5. Run `docker compose build`
6. Run `docker compose up`
