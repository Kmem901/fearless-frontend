import React from 'react';

class ConferenceForm extends React.Component {
    constructor(props) {
        super(props)
        this.state = {
            name: "",
            starts: "",
            ends: "",
            description: "",
            maxPresentations: "",
            maxAttendees: "",
            locations: []
        };
        this.handleNameChange = this.handleNameChange.bind(this);
        this.handleStartsChange = this.handleStartsChange.bind(this);
        this.handleEndsChange = this.handleEndsChange.bind(this);
        this.handleDescriptionChange = this.handleDescriptionChange.bind(this);
        this.handleMaxAttendeesChange = this.handleMaxAttendeesChange.bind(this);
        this.handleMaxPresentationChange = this.handleMaxPresentationChange.bind(this);
        this.handleLocationChange = this.handleLocationChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);

    }

    handleNameChange(event) {
        const value = event.target.value;
        this.setState({name: value})
        // console.log(value)
    }

    handleStartsChange(event) {
        const value = event.target.value;
        this.setState({starts: value})
        // console.log(value)
    }

    handleEndsChange(event) {
        const value = event.target.value;
        this.setState({ends: value})
        // console.log(value)
    }

    handleDescriptionChange(event) {
        const value = event.target.value;
        this.setState({description: value})
        // console.log(value)
    }

    handleMaxPresentationChange(event) {
        const value = event.target.value;
        this.setState({maxPresentations: value})
        // console.log(value)
    }

    handleMaxAttendeesChange(event) {
        const value = event.target.value;
        this.setState({maxAttendees: value})
        // console.log(value)
    }

    handleLocationChange(event) {
        const value = event.target.value;
        this.setState({location: value})
        // console.log(value)
    }

    async handleSubmit(event) {
        event.preventDefault();
        console.log(this.state)
        const data = {...this.state};
        data.max_presentations = data.maxPresentations;
        data.max_attendees = data.maxAttendees;
        delete data.maxPresentations;
        delete data.maxAttendees;
        delete data.locations;
        console.log(data);

        const conferenceUrl = 'http://localhost:8000/api/conferences/';
        const fetchConfig = {
          method: "post",
          body: JSON.stringify(data),
          headers: {
              'Content-Type': 'application/json',
          },
        };
        const response = await fetch(conferenceUrl, fetchConfig);
        if (response.ok) {
          const newConference = await response.json();
          console.log(newConference);

          const cleared = {
            name: "",
            starts: "",
            ends: "",
            description: "",
            maxPresentations: "",
            maxAttendees: "",
            location: "",
            };
            this.setState(cleared);
        }
    }

    async componentDidMount() {
        const url = "http://localhost:8000/api/locations/";

        const response = await fetch(url);

        if (response.ok) {
            const data = await response.json();
            console.log(data)
            this.setState({locations: data.locations})
            }
    }

    render() {
        return (
        <div className="row">
            <div className="offset-3 col-6">
                <div className="shadow p-4 mt-4">
                    <h1>Create a new conference</h1>
                        <form onSubmit={this.handleSubmit} id="create-conference-form">
                        <div className="form-floating mb-3">
                            <input value={this.state.name} onChange={this.handleNameChange} placeholder="Name" required type="text" name="name" id="name" className="form-control"/>
                            <label htmlFor="name">Name</label>
                        </div>
                        <div className="form-floating mb-3">
                            <input value={this.state.starts} onChange={this.handleStartsChange} placeholder="Starts" required type="date" name="starts" id="starts" className="form-control"/>
                            <label htmlFor="starts">Starts</label>
                        </div>
                        <div className="form-floating mb-3">
                            <input value={this.state.ends} onChange={this.handleEndsChange} placeholder="Ends" required type="date" name="ends" id="ends" className="form-control"/>
                            <label htmlFor="ends">Ends</label>
                        </div>
                        <div className="form mb-3">
                            <label htmlFor="Description">Description</label>
                            <textarea value={this.state.description} onChange={this.handleDescriptionChange} name="description" id="description" cols="10" rows="4" className="form-control"></textarea>
                        </div>
                        <div className="form-floating mb-3">
                            <input value={this.state.maxPresentations} onChange={this.handleMaxPresentationChange} placeholder="Max presentations" required type="number" name="max_presentations" id="max_presentations" className="form-control"/>
                            <label htmlFor="max_presentations">Max presentations</label>
                        </div>
                        <div className="form-floating mb-3">
                            <input value={this.state.maxAttendees} onChange={this.handleMaxAttendeesChange} placeholder="Max attendees" required type="number" name="max_attendees" id="max_attendees" className="form-control"/>
                            <label htmlFor="max_attendees">Max attendees</label>
                        </div>
                        <div className="mb-3">
                            <select value={this.state.location} onChange={this.handleLocationChange}required name="location" id="location" className="form-select">
                            <option value="">Choose a location</option>
                            {this.state.locations.map(location => {
                                return (
                                    <option key={location.id} value={location.id}>
                                        {location.name}
                                    </option>
                                );
                            })}
                            </select>
                        </div>
                        <button className="btn btn-primary">Create</button>
                        </form>
                    </div>
                </div>
            </div>
        );
    }
}


export default ConferenceForm
